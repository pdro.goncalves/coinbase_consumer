Coinbase Watcher
================

Overview
--------

A GO service that subscribes the [Coinbase Market Data API Websocket](https://docs.cloud.coinbase.com/exchange/docs/websocket-overview) and consume its messages.


To configure what Channels and Products to watch edit the **.env** file.
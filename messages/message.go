package coinbase

type Message struct {
	/* Message struct to send messages to Websocket
	https://docs.cloud.coinbase.com/exchange/docs/websocket-overview#subscribe
	https://docs.cloud.coinbase.com/exchange/docs/websocket-overview#unsubscribe
	*/
	Type       string   `json:"type"`
	ProductIds []string `json:"product_ids"`
	Channels   []string `json:"channels"`
}

type HeartbeatMessage struct {
	/* Message struct for heartbeat type messages:
	https://docs.cloud.coinbase.com/exchange/docs/websocket-channels#heartbeat-channel
	*/
	Type        string `json:"type"`
	Sequence    int    `json:"sequence"`
	LastTradeId int    `json:"last_trade_id"`
	ProductId   string `json:"product_id"`
	Time        string `json:"time"`
}

type TickerMessage struct {
	/* Message struct for ticker type messages:
	https://docs.cloud.coinbase.com/exchange/docs/websocket-channels#ticker-channel
	*/
	Type        string  `json:"type"`
	Sequence    int     `json:"sequence"`
	ProductId   string  `json:"product_id"`
	Price       float32 `json:"price"`
	Open24h     float32 `json:"open_24h"`
	Volume24h   float32 `json:"volume_24h"`
	Low24h      float32 `json:"low_24h"`
	High24h     float32 `json:"high_24h"`
	Volume30d   float64 `json:"volume_30d"`
	BestBid     float32 `json:"best_bid"`
	BestBidSize float32 `json:"best_bid_size"`
	BestAsk     float32 `json:"best_ask"`
	BestAskSize float32 `json:"best_ask_size"`
	Side        string  `json:"side"`
	Time        string  `json:"time"`
	TradeId     int     `json:"trade_id"`
	LastSize    float32 `json:"last_size"`
}

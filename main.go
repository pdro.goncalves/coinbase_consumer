package main

import (
	coinbase "coinbase_consumer/messages"
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"strings"

	"github.com/joho/godotenv"
	"golang.org/x/net/websocket"
)

func sendMessage(ws *websocket.Conn, m coinbase.Message) error {

	msg, err := json.Marshal(m)
	if err != nil {
		log.Fatal(err)
		return err
	}
	if _, err := ws.Write(msg); err != nil {
		log.Fatal(err)
		return err
	}
	log.Printf("Sent message: %s\n", msg)

	return nil
}

func readMessage(ws *websocket.Conn, incomingMessages chan []byte) {

	for {
		var msg = make([]byte, 512)
		var n int
		n, err := ws.Read(msg)
		if err != nil {
			log.Fatal(err)
		}
		incomingMessages <- msg[:n]
	}

}

func parseMessage(m []byte) {
	// a map container to decode the JSON structure into
	c := make(map[string]json.RawMessage)

	// unmarschal JSON
	error := json.Unmarshal(m, &c)
	if error != nil {
		log.Fatal(error)
	}

	log.Printf("Message type: %s", c["type"])
	// // a string slice to hold the keys
	// k := make([]string, len(c))

	// // iteration counter
	// i := 0

	// // copy c's keys into k
	// for s, _ := range c {
	// 	k[i] = s
	// 	i++
	// }

	// // output result to STDOUT
	// fmt.Printf("%#v\n", k)
}

func init() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
}

func main() {

	products := strings.Split(os.Getenv("PRODUCTS"), ",")
	channels := strings.Split(os.Getenv("CHANNELS"), ",")
	feed := os.Getenv("FEED_URI")

	log.Printf("Starting Coinbase Watcher")
	log.Printf("Feed URL: %s", feed)
	log.Printf("Products: %s", products)
	log.Printf("Channels: %s", channels)

	ws, err := websocket.Dial(feed, "", "http://localhost")
	if err != nil {
		log.Fatal(err)
	}

	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)

	signal.Notify(sigs, os.Interrupt)
	go func() {
		sig := <-sigs
		log.Printf("Received signal: %s", sig)
		log.Printf("Sending unsubscribe message")
		u_message := coinbase.Message{
			Type:       "unsubscribe",
			ProductIds: products,
			Channels:   channels,
		}
		sendMessage(ws, u_message)
		if err != nil {
			log.Fatal(err)
		}
		done <- true
	}()

	m := coinbase.Message{
		Type:       "subscribe",
		ProductIds: products,
		Channels:   channels,
	}
	log.Printf("Sending subscribe message")
	err = sendMessage(ws, m)
	if err != nil {
		log.Fatal("Error sending subscribe message")
		log.Fatal(err)
	}

	log.Printf("Subscribed...")
	log.Printf("Start reading messages...")
	incommingMessages := make(chan []byte)
	go readMessage(ws, incommingMessages)
	for {
		select {
		case m := <-incommingMessages:
			log.Printf("Message received: %s\n", m)
			parseMessage(m)
		case <-done:
			log.Printf("Bye...")
			os.Exit(0)
		}

	}
}
